import React, { useContext } from "react";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import { TodosContext } from "./contexts/todos.context";
import Divider from "@mui/material/Divider";
import Todo from "./Todo";

function TodoList() {
  const todos = useContext(TodosContext);
  const todoList = todos;

  const list = todoList.map((todo, idx) => {
    return (
      <Box key={idx}>
        <Todo todo={todo} />
        {todoList.length - 1 > idx && <Divider />}
      </Box>
    );
  });

  return (
    <Paper>
      <List>{list}</List>
    </Paper>
  );
}
export default TodoList;
