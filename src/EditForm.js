import { TextField } from "@mui/material";
import React from "react";
import useImputState from "./hooks/useInputState";
import { DispatchContext } from "./contexts/todos.context";
import { useContext } from "react";

function EditForm(props) {
  const dispatch = useContext(DispatchContext);

  const handleSaveEdit = (e) => {
    e.preventDefault();
    dispatch({ type: "EDIT", id: props.todo.id, newTask: value });
    reset();
    props.setEditMode(value);
  };

  const [value, setValue, reset] = useImputState(props.task);

  return (
    <form
      onSubmit={handleSaveEdit}
      style={{ marginLeft: "1rem", marginBottom: "5px", width: "80%" }}
    >
      <TextField
        fullWidth
        margin="normal"
        label="Edit Task"
        color="secondary"
        value={value}
        onChange={setValue}
        autoFocus
      />
    </form>
  );
}
export default EditForm;
