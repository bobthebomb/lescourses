import React, { useContext } from "react";
import Paper from "@mui/material/Paper";
import useImputState from "./hooks/useInputState";
import { TextField } from "@mui/material";
import { DispatchContext } from "./contexts/todos.context";

function TodoForm() {
  const [value, handleValueChange, reset] = useImputState("");
  const dispatch = useContext(DispatchContext);

  const handleSave = (e) => {
    e.preventDefault();
    if (value.length > 0) {
      dispatch({ type: "ADD", task: value });
      reset();
    }
  };
  return (
    <Paper sx={{ margin: "1rem 0", padding: "0 1rem" }}>
      <form onSubmit={handleSave}>
        <TextField
          margin="normal"
          fullWidth
          label="Add New Task"
          color="secondary"
          value={value}
          onChange={handleValueChange}
        />
      </form>
    </Paper>
  );
}
export default TodoForm;
