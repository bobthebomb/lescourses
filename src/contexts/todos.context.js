//todos
//all methods to interact with todos

import React, { createContext, useReducer } from "react";
import todoReducer from "../reducers/todo.reducer";
import  useLocalStorageReducer  from "../hooks/useLocalStorageReducer";

const defaultTodos = [
  { id: 1, task: "Mow the law using goats", completed: false },
  { id: 2, task: "Release lady bugs into garden", completed: true },
];
export const TodosContext = createContext(defaultTodos);
export const DispatchContext = createContext();

//one function can pass de .Provider to avoid multiple rendering.
export function TodosProvider(props) {
  const [todos, dispatch] = useLocalStorageReducer(
    "todos",
    defaultTodos,
    todoReducer
  );
  return (
    <TodosContext.Provider value={todos}>
      <DispatchContext.Provider value={dispatch}>
        {props.children}
      </DispatchContext.Provider>
    </TodosContext.Provider>
  );
}
