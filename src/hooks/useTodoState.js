import { v4 as uuidv4 } from "uuid";
import useLocalStorageState from "./useLocalStorageState";

function useTodoState(initialValue) {
  const [todos, setTodos] = useLocalStorageState("todos", initialValue);

  const removeTodoHook = (todoId) => {
    const updatedTodo = todos.filter((todo) => todo.id !== todoId);
    setTodos(updatedTodo);
  };

  const toggleTodoHook = (todoId) => {
    const updatedTodo = todos.map((todo) =>
      todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
    );
    setTodos(updatedTodo);
  };

  const addTodoHook = (newTodoText) => {
    setTodos([...todos, { id: uuidv4(), task: newTodoText, completed: false }]);
  };

  const editTodoHook = (passedEditedTodo) => {
    const editedTodo = todos.map((todo) =>
      passedEditedTodo.id === todo.id
        ? { ...todo, task: passedEditedTodo.task }
        : todo
    );
    setTodos(editedTodo);
  };

  return {
    todos,
    addTodo: addTodoHook,
    removeTodo: removeTodoHook,
    toggleTodo: toggleTodoHook,
    editTodo: editTodoHook,
  };
}
export default useTodoState;
