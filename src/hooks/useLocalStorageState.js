import { useState, useEffect } from "react";

function useLocalStorageState(key, defaultVal) {
  const [state, setState] = useState(() => {
    let val;
    try {
      val = JSON.parse(window.localStorage.getItem(key) || String(defaultVal));
    } catch (e) {
      val = defaultVal;
    }
    return val;
  });

  useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(state));
  }, [state]);

  return [state, setState];
}

export default useLocalStorageState;

// const initialTodos = JSON.parse(window.localStorage.getItem("todos"))
//     const loadWith = initialTodos.length > 0 ? initialTodos : props.initialTask

//     const { todos, addTodo, removeTodo,  toggleTodo, editTodo  } = useTodoState(loadWith)

//     const gridStyle = { marginTop: "1rem"}

//     useEffect( ()=> {
//         window.localStorage.setItem('todos', JSON.stringify(todos))
//     }, [todos])
