import React, { useContext, memo } from "react";
import ListItemText from "@mui/material/ListItemText";
import ListItem from "@mui/material/ListItem";
import Checkbox from "@mui/material/Checkbox";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import useToggle from "./hooks/useToggle";
import EditForm from "./EditForm";
import ListItemIcon from "@mui/material/ListItemIcon";
import { DispatchContext } from "./contexts/todos.context";

function Todo(props) {
  const dispatch = useContext(DispatchContext);
  const { task, id, completed } = props.todo;

  const handleClickDelete = () => {
    dispatch({ type: "REMOVE", id: id });
  };

  const handleToogle = () => {
    dispatch({ type: "TOGGLE", id: id });
  };

  const handleEdit = () => {
    setEditMode();
  };

  const [editMode, setEditMode] = useToggle(false);
  return (
    <ListItem>
      {editMode ? (
        <EditForm todo={props.todo} task={task} setEditMode={setEditMode} />
      ) : (
        <>
          <ListItemIcon>
            <Checkbox
              tabIndex={-1}
              checked={completed}
              color="success"
              onClick={handleToogle}
            />
          </ListItemIcon>
          <ListItemText
            sx={{ textDecoration: completed ? "line-through" : "none" }}
          >
            {task}
          </ListItemText>

          <IconButton aria-label="delete" onClick={handleClickDelete}>
            <DeleteIcon />
          </IconButton>
          <IconButton aria-label="edit" onClick={handleEdit}>
            <EditIcon />
          </IconButton>
        </>
      )}
    </ListItem>
  );
}
export default memo(Todo);
