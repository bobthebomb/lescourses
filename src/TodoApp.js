import React from "react";
import { AppBar, Paper, Toolbar, Typography, Grid } from "@mui/material";
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";
import { TodosProvider } from "./contexts/todos.context";

function TodoApp(props) {
  const gridStyle = { marginTop: "1rem" };

  return (
    <Paper sx={{ background: "Ghostwhite", height: "100vh" }}>
      <AppBar color="primary" position="static" style={{ height: "64px" }}>
        <Toolbar>
          <Typography color="inherit"> Liste des Courses </Typography>
        </Toolbar>
      </AppBar>

      <Grid sx={gridStyle} container justifyContent="center">
        <Grid item xs={11} md={8} lg={4}>
          <TodosProvider>
            <TodoForm />

            <TodoList />
          </TodosProvider>
        </Grid>
      </Grid>
    </Paper>
  );
}
export default TodoApp;
