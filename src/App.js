import "./App.css";
import TodoApp from "./TodoApp.js";

function App() {
  const initialTask = [{ id: 1, task: "Exemple d element ", completed: false }];

  return <TodoApp initialTask={initialTask} />;
}

export default App;
